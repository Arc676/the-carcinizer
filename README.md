# The Carcinizer

A Discord bot written in Rust with no individual purpose.

## Configuration and Setup

The Carcinizer expects to find its bot token in a file named `.token` in the working directory. Configuration can be specified in a file named `carcinizer.json`, also in the working directory. This file can specify the following settings:

| Key      | Datatype            | Description                                                        |
|----------|---------------------|--------------------------------------------------------------------|
| `admins` | Array (of user IDs) | Specifies which users are to be considered owners                  |
| `logs`   | String              | Filename for logfile in which to append incident and error reports |

## Licensing

Source code available under GPLv3. Bot icon based on [cuddlyferris](https://rustacean.net/assets/cuddlyferris.png) available under CC0, just like the original file.

Hangman states adapted from [this image](https://www.flaticon.com/free-icon/hangman-game_2241186?term=hangman&page=1&position=6) by [smalllikeart](https://www.flaticon.com/authors/smalllikeart) on [flaticon](https://www.flaticon.com/). Modified versions available under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) (modifications by Arc676/Alessandro Vinciguerra). Original available under Terms of use by Flaticon.
