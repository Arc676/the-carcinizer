// Copyright (C) 2022 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3).

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use crate::{log, parse_id};
use serde::{Deserialize, Serialize};
use serenity::client::Context;
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{Args, CommandResult};
use serenity::futures::StreamExt;
use serenity::model::prelude::{ChannelId, Message, UserId};
use serenity::prelude::TypeMapKey;

#[group]
#[commands(count, stop_count, count_status)]
struct Counting;

#[derive(Clone, Default, Serialize, Deserialize)]
pub struct CountSetup {
    listen: UserId,
    channel: ChannelId,
    #[serde(skip)]
    last_value: u64,
}

impl TypeMapKey for CountSetup {
    type Value = CountSetup;
}

pub const ERR_NO_COUNTING: &str = "No counting setup found in bot";

macro_rules! get_mgr_mut {
    ($ctx:ident, $data:ident, $mgr:ident) => {
        let mut $data = $ctx.data.write().await;
        let $mgr = $data.get_mut::<CountSetup>().expect(ERR_NO_COUNTING);
    };
}

macro_rules! get_mgr {
    ($ctx:ident, $data:ident, $mgr:ident) => {
        let $data = $ctx.data.read().await;
        let $mgr = $data.get::<CountSetup>().expect(ERR_NO_COUNTING);
    };
}

pub async fn init_counting(ctx: &Context, error_channel: Option<ChannelId>) {
    get_mgr_mut!(ctx, data, counting);
    if counting.listen == UserId::default() {
        return;
    }

    let mut messages = counting.channel.messages_iter(&ctx).boxed();
    let mut found_count = false;
    let mut searched = 0;
    while let Some(m) = messages.next().await {
        if let Ok(msg) = m {
            searched += 1;
            if msg.author.id == ctx.cache.current_user_id() {
                return;
            }
            if msg.author.id == counting.listen {
                found_count = true;
                if let Ok(value) = msg.content.parse::<u64>() {
                    counting.last_value = value + 1;
                }
                break;
            }
        }
        if searched >= 10 {
            break;
        }
    }
    if found_count {
        let (channel, value) = (counting.channel, counting.last_value);
        let data = data.downgrade();
        for _ in 0..5 {
            if let Err(e) = channel.say(&ctx.http, format!("{}", value)).await {
                log(&data, e);
            } else {
                break;
            }
        }
    } else if let Some(channel) = error_channel {
        if let Err(e) = channel
                .say(
                    &ctx.http,
                    format!(
                        "Count not restart counting: no numerical message from <@{}> in the {} most recent messages in <#{}>",
                        counting.listen, searched, counting.channel
                    ),
                )
                .await
            {
                log(&data.downgrade(), e);
            }
    }
}

pub async fn count_handler(ctx: &Context, msg: &Message) {
    get_mgr_mut!(ctx, data, counting);
    if msg.channel_id == counting.channel && msg.author.id == counting.listen {
        if let Ok(value) = msg.content.parse::<u64>() {
            let new = value + 1;
            counting.last_value = new;
            let data = data.downgrade();
            for _ in 0..5 {
                if let Err(e) = msg.channel_id.say(&ctx.http, format!("{}", new)).await {
                    log(&data, e);
                } else {
                    break;
                }
            }
        }
    }
}

#[command]
#[description("Sets the bot counting following a given user in a given channel")]
#[usage("#channel @user")]
#[num_args(2)]
async fn count(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    get_mgr_mut!(ctx, data, setup);
    setup.channel = parse_id(&mut args)?;
    setup.listen = parse_id(&mut args)?;
    msg.reply(
        ctx,
        format!(
            "Counting in channel <#{}> following user <@{}>",
            setup.channel, setup.listen
        ),
    )
    .await?;
    Ok(())
}

#[command]
#[description("Stops bot counting")]
async fn stop_count(ctx: &Context, msg: &Message) -> CommandResult {
    get_mgr_mut!(ctx, data, setup);
    setup.listen = UserId::default();
    setup.channel = ChannelId::default();
    msg.reply(ctx, "No longer counting").await?;
    Ok(())
}

#[command]
#[description("Shows counting status")]
async fn count_status(ctx: &Context, msg: &Message) -> CommandResult {
    get_mgr!(ctx, data, setup);
    if setup.listen == UserId::default() {
        msg.reply(ctx, "No counting in progress").await?;
    } else {
        msg.reply(
            ctx,
            format!(
                "Currently counting after <@{}> in <#{}> (last value: {})",
                setup.listen, setup.channel, setup.last_value
            ),
        )
        .await?;
    }
    Ok(())
}
