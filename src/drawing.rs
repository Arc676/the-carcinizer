// Copyright (C) 2022 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3).

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use std::fs;
use std::io::Cursor;

use image::imageops::Gaussian;
use image::io::Reader;
use image::{ImageError, RgbaImage};
use serde::{Deserialize, Serialize};
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{Args, CommandResult};
use serenity::model::id::ChannelId;
use serenity::model::prelude::{Message, UserId};
use serenity::prelude::{Context, TypeMapKey};
use serenity::utils::Color;
use serenity::Error;
use tokio::task::JoinHandle;

use crate::{is_owner, log, parse_id, save_state, ACCOUNTABILITY_CHECK};

#[group]
#[owner_privilege]
#[commands(
    draw_autostart,
    draw_skip,
    draw_stop,
    draw_status,
    draw_submit,
    draw_command,
    draw_list,
    draw_start,
    draw_view,
    draw_clear,
    draw_remove
)]
struct Drawing;

#[derive(Clone, Serialize, Deserialize)]
struct DrawStatus {
    name: String,
    location: (u32, u32),
    current: (u32, u32),
    filename: String,
    owner: UserId,
    submission_channel: ChannelId,
    #[serde(skip)]
    image: RgbaImage,
}

#[derive(Default, Serialize, Deserialize)]
pub struct DrawingManager {
    command: String,
    projects: Vec<DrawStatus>,
    active: Option<usize>,
    autostart: bool,
    skip_job: bool,
    channel: ChannelId,
    #[serde(skip)]
    handles: Vec<JoinHandle<Result<(), Error>>>,
}

impl TypeMapKey for DrawingManager {
    type Value = DrawingManager;
}

impl Clone for DrawingManager {
    fn clone(&self) -> Self {
        DrawingManager {
            command: self.command.clone(),
            projects: self.projects.clone(),
            handles: vec![],
            ..*self
        }
    }
}

pub const ERR_NO_DRAWING: &str = "No drawing manager in bot";

macro_rules! get_mgr_mut {
    ($ctx:ident, $data:ident, $mgr:ident) => {
        let mut $data = $ctx.data.write().await;
        let $mgr = $data.get_mut::<DrawingManager>().expect(ERR_NO_DRAWING);
    };
}

macro_rules! get_mgr {
    ($ctx:ident, $data:ident, $mgr:ident) => {
        let $data = $ctx.data.read().await;
        let $mgr = $data.get::<DrawingManager>().expect(ERR_NO_DRAWING);
    };
}

pub async fn init_drawing(ctx: &Context) {
    get_mgr_mut!(ctx, data, mgr);
    if let Some(idx) = mgr.active {
        if let Err(e) = start_project(ctx, mgr, idx).await {
            log(&data.downgrade(), e);
        }
    }
}

pub async fn check_projects(ctx: &Context) {
    get_mgr_mut!(ctx, data, mgr);
    if mgr.active.is_none() && mgr.autostart && !mgr.projects.is_empty() {
        if let Err(e) = start_project(ctx, mgr, 0).await {
            log(&data.downgrade(), e);
        }
    }
}

pub async fn load_projects(ctx: &Context) -> Result<(), ImageError> {
    get_mgr_mut!(ctx, data, mgr);
    for project in &mut mgr.projects {
        let img = Reader::open(&project.filename)?.decode()?;
        project.image = img.to_rgba8();
    }
    Ok(())
}

async fn start_project(ctx: &Context, mgr: &mut DrawingManager, idx: usize) -> CommandResult {
    let ctx_copy = ctx.clone();
    let cmd_copy = mgr.command.clone();
    let project_copy = mgr.projects[idx].clone();
    let channel = mgr.channel;

    project_copy
        .submission_channel
        .send_message(&ctx.http, |m| {
            m.content(format!(
                "<@{}> Now drawing `{}`",
                project_copy.owner, &project_copy.name
            ))
        })
        .await?;

    let handle = tokio::spawn(draw_project(ctx_copy, channel, cmd_copy, project_copy));
    mgr.handles.push(handle);
    mgr.active = Some(idx);

    Ok(())
}

async fn draw_project(
    ctx: Context,
    channel: ChannelId,
    cmd: String,
    project: DrawStatus,
) -> Result<(), Error> {
    let img = project.image;
    let (px, py) = project.location;
    let mut skipped = false;
    for y in project.current.1..img.height() {
        for x in project.current.0..img.width() {
            let pixel = img[(x, y)];
            if pixel[3] == 0 {
                continue;
            }
            let (dx, dy) = (x + px, y + py);
            channel
                .send_message(&ctx.http, |m| {
                    m.content(format!(
                        "{} {} {} #{:02X}{:02X}{:02X}",
                        &cmd, dx, dy, pixel[0], pixel[1], pixel[2]
                    ))
                })
                .await?;

            get_mgr_mut!(ctx, data, mgr);
            if mgr.skip_job {
                skipped = true;
                break;
            }
            for stored in &mut mgr.projects {
                if stored.name == project.name {
                    stored.current = (x, y);
                    break;
                }
            }
        }
        if skipped {
            break;
        }
        save_state(&ctx).await;
    }
    get_mgr_mut!(ctx, data, mgr);

    project
        .submission_channel
        .send_message(&ctx.http, |m| {
            m.content(format!(
                "<@{}> {} drawing `{}`",
                project.owner,
                if skipped { "Skipped" } else { "Finished" },
                &project.name
            ))
        })
        .await?;

    mgr.projects.retain(|job| job.name != project.name);
    mgr.active = None;
    mgr.skip_job = false;

    fs::remove_file(&project.filename)?;

    drop(data);
    save_state(&ctx).await;
    Ok(())
}

#[command]
#[owners_only]
#[description("Starts a queued job")]
#[usage("job_name")]
#[num_args(1)]
async fn draw_start(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let name = args.single::<String>()?;
    get_mgr_mut!(ctx, data, mgr);
    let idx = mgr.projects.iter().position(|job| job.name == name);
    match idx {
        None => {
            if let Err(e) = msg.reply(&ctx, "Failed to find job").await {
                log(&data.downgrade(), e);
            }
        }
        Some(idx) => {
            if mgr.command.is_empty() || mgr.channel == ChannelId::default() {
                if let Err(e) = msg
                    .reply(
                        &ctx,
                        "Cannot draw: drawing channel and/or drawing command not set",
                    )
                    .await
                {
                    log(&data.downgrade(), e);
                }
                return Ok(());
            }
            start_project(ctx, mgr, idx).await?;
        }
    }
    Ok(())
}

#[command]
#[owners_only]
#[description("Sets the command for drawing. If no arguments are given, shows the current configuration instead.")]
#[usage("[#channel command...]")]
async fn draw_command(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    get_mgr_mut!(ctx, data, mgr);

    if args.is_empty() {
        msg.reply(
            &ctx,
            format!(
                "Current configuration: `{} x y hex` in <#{}>",
                &mgr.command, mgr.channel
            ),
        )
        .await?;
        return Ok(());
    }
    let channel = parse_id(&mut args)?;

    mgr.channel = ChannelId(channel);
    mgr.command = args.rest().to_string();
    msg.reply(
        ctx,
        format!(
            "Now drawing in <#{}> using command `{} x y hex`",
            channel, &mgr.command
        ),
    )
    .await?;
    drop(data);
    save_state(ctx).await;
    Ok(())
}

#[command]
#[description("Submits the attached image for drawing")]
#[usage("name X Y W H")]
#[num_args(5)]
#[checks(Accountability)]
async fn draw_submit(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let name = args.single::<String>()?;
    let x = args.single::<u32>()?;
    let y = args.single::<u32>()?;
    let w = args.single::<u32>()?;
    let h = args.single::<u32>()?;
    match msg.attachments.first() {
        None => {
            msg.reply(ctx, "No image attached").await?;
        }
        Some(attachment) => {
            let contents = match attachment.download().await {
                Ok(bytes) => bytes,
                Err(e) => {
                    msg.reply(ctx, format!("Failed to download attachment: {}", e))
                        .await?;
                    return Ok(());
                }
            };
            let image = Reader::new(Cursor::new(contents))
                .with_guessed_format()?
                .decode()?
                .resize(w, h, Gaussian)
                .to_rgba8();
            match image.save(&attachment.filename) {
                Ok(_) => {
                    get_mgr_mut!(ctx, data, mgr);

                    if mgr.projects.iter().any(|job| job.name == name) {
                        msg.reply(ctx, "Duplicate job name. Try again with a new name.")
                            .await?;
                        return Ok(());
                    }

                    let job = DrawStatus {
                        name,
                        image,
                        owner: msg.author.id,
                        submission_channel: msg.channel_id,
                        filename: attachment.filename.clone(),
                        current: (0, 0),
                        location: (x, y),
                    };
                    mgr.projects.push(job);

                    msg.reply(
                        ctx,
                        format!("Downloaded image {} and queued job", &attachment.filename),
                    )
                    .await?;
                }
                Err(e) => {
                    msg.reply(ctx, format!("Failed to save attachment: {}", e))
                        .await?;
                }
            }
        }
    }
    Ok(())
}

#[command]
#[description("Lists queued drawings")]
async fn draw_list(ctx: &Context, msg: &Message) -> CommandResult {
    get_mgr!(ctx, data, mgr);
    msg.channel_id
        .send_message(&ctx.http, |m| {
            m.reference_message(msg).embed(|e| {
                e.title("Queued jobs");
                e.color(Color::from_rgb(0, 216, 105));
                e.description(format!("{} queued job(s)", mgr.projects.len()));
                for project in &mgr.projects {
                    let (w, h) = project.image.dimensions();
                    let (x, y) = project.location;
                    e.field(
                        &project.name,
                        format!("{}x{} at ({}, {})", w, h, x, y),
                        false,
                    );
                }
                e
            })
        })
        .await?;
    Ok(())
}

fn percentage_drawn(job: &DrawStatus) -> (u32, u32, f32) {
    let (w, h) = job.image.dimensions();
    let total = w * h;
    let (dx, dy) = job.current;
    let drawn = dy * w + dx;
    (drawn, total, drawn as f32 / total as f32 * 100.)
}

#[command]
#[description("Shows drawing status")]
async fn draw_status(ctx: &Context, msg: &Message) -> CommandResult {
    get_mgr!(ctx, data, mgr);
    match &mgr.active {
        None => msg.reply(ctx, "No drawing in progress").await?,
        Some(idx) => match mgr.projects.get(*idx) {
            None => msg.reply(ctx, "Failed to get current job").await?,
            Some(job) => {
                let (drawn, total, percentage) = percentage_drawn(job);
                msg.reply(
                    ctx,
                    format!(
                        "Currently drawing `{}`: {}/{} pixels drawn ({:.1}%)",
                        job.name, drawn, total, percentage
                    ),
                )
                .await?
            }
        },
    };
    Ok(())
}

#[command]
#[description("Removes the given jobs from the queue")]
#[usage("job [jobs...]")]
async fn draw_remove(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut names = args.iter().map(|x| x.unwrap()).collect::<Vec<String>>();
    let is_owner = is_owner(ctx, msg.author.id).await;
    get_mgr_mut!(ctx, data, mgr);
    if let Some(idx) = mgr.active {
        if mgr.projects[idx].owner != msg.author.id && !is_owner {
            msg.reply(ctx, "You cannot delete this job").await?;
            return Ok(());
        }
        let active = &mgr.projects[idx].name;
        names.retain(|name| name != active);
    }
    mgr.projects
        .retain(|job| !names.contains(&job.name) || fs::remove_file(&job.filename).is_err());
    msg.reply(&ctx, format!("Deleted {} job(s) from queue", names.len()))
        .await?;
    Ok(())
}

#[command]
#[description("Removes all inactive jobs from the queue")]
#[owners_only]
async fn draw_clear(ctx: &Context, msg: &Message) -> CommandResult {
    get_mgr_mut!(ctx, data, mgr);
    let active = match mgr.active {
        None => None,
        Some(idx) => Some(mgr.projects.remove(idx)),
    };
    let removed = mgr.projects.len();
    for (i, job) in mgr.projects.iter().enumerate() {
        if mgr.active != Some(i) {
            fs::remove_file(&job.filename)?;
        }
    }
    mgr.projects.clear();
    if let Some(job) = active {
        mgr.projects.push(job);
    }
    msg.reply(&ctx, format!("Deleted {} job(s) from queue", removed))
        .await?;
    Ok(())
}

#[command]
#[description("Shows information and a preview of the given job")]
#[usage("job_name")]
#[num_args(1)]
async fn draw_view(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let name = args.single::<String>()?;
    get_mgr!(ctx, data, mgr);
    let job = mgr.projects.iter().find(|j| j.name == name);
    match job {
        None => msg.reply(&ctx, "No job with the given name").await?,
        Some(job) => {
            let (w, h) = job.image.dimensions();
            let (x, y) = job.location;
            let (_, _, percentage) = percentage_drawn(job);
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    m.reference_message(msg)
                        .embed(|e| {
                            e.title(format!("Job info for {}", name))
                                .description(format!("{}x{} image at ({}, {})", w, h, x, y))
                                .field("Submitted by", format!("<@{}>", job.owner), false)
                                .field(
                                    "Percentage of image drawn",
                                    format!("{:.1}%", percentage),
                                    false,
                                )
                                .image(format!("attachment://{}", &job.filename))
                        })
                        .add_file(&*job.filename)
                })
                .await?
        }
    };
    Ok(())
}

#[command]
#[description("Sets whether the next queued drawing job should start automatically")]
#[usage("yes|no")]
#[num_args(1)]
#[owners_only]
async fn draw_autostart(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let setting = args.single::<String>()?;
    get_mgr_mut!(ctx, data, mgr);
    mgr.autostart = setting.to_ascii_lowercase() == "yes";
    msg.reply(ctx, format!("Autostart set to {}", mgr.autostart))
        .await?;
    drop(data);
    save_state(ctx).await;
    Ok(())
}

#[command]
#[description("Skips straight to the next drawing job")]
async fn draw_skip(ctx: &Context, msg: &Message) -> CommandResult {
    let is_owner = is_owner(ctx, msg.author.id).await;
    get_mgr_mut!(ctx, data, mgr);
    if let Some(idx) = mgr.active {
        if mgr.projects[idx].owner != msg.author.id && !is_owner {
            msg.reply(ctx, "You cannot skip this job").await?;
            return Ok(());
        }
        mgr.skip_job = true;
        msg.reply(
            ctx,
            format!("Skipping drawing `{}`", &mgr.projects[idx].name),
        )
        .await?;
    } else {
        msg.reply(ctx, "No drawing in progress").await?;
    }
    Ok(())
}

#[command]
#[owners_only]
#[description("Stops the current drawing job and prevents the next one from being started")]
async fn draw_stop(ctx: &Context, msg: &Message) -> CommandResult {
    get_mgr_mut!(ctx, data, mgr);
    mgr.autostart = false;
    mgr.skip_job = true;
    if let Some(idx) = mgr.active {
        msg.reply(
            ctx,
            format!(
                "Stopping drawing `{}` and disabled drawing autostart",
                &mgr.projects[idx].name
            ),
        )
        .await?;
    } else {
        msg.reply(ctx, "Drawing autostart disabled").await?;
    }
    Ok(())
}
