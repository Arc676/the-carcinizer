// Copyright (C) 2020 Fatcat560/Mario Spies
// Copyright (C) 2020-2 Arc676/Alessandro Vinciguerra

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3)

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::fs::File;
use std::io::Write;
use std::{char, fs};

use std::sync::Arc;

use crate::log;
use hangman::hangman::Hangman;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{Args, CommandResult};
use serenity::model::channel::{Reaction, ReactionType};
use serenity::model::id::ChannelId;
use serenity::model::prelude::Message;
use serenity::prelude::{Context, Mentionable, Mutex, TypeMapKey};

#[group]
#[commands(start_hangman, create_hangman, random_hangman, wordlist, hangman_lists)]
struct Executioner;

#[derive(Default, Serialize, Deserialize)]
pub struct HangmanManager {
    wordlists: Vec<String>,
    #[serde(skip)]
    games: Arc<Mutex<HashMap<String, GameRound>>>,
}

impl TypeMapKey for HangmanManager {
    type Value = HangmanManager;
}

impl Clone for HangmanManager {
    fn clone(&self) -> Self {
        HangmanManager {
            wordlists: self.wordlists.clone(),
            ..Default::default()
        }
    }
}

const LETTERS: [&str; 26] = [
    "🇦", "🇧", "🇨", "🇩", "🇪", "🇫", "🇬", "🇭", "🇮", "🇯", "🇰", "🇱", "🇲", "🇳", "🇴", "🇵", "🇶", "🇷", "🇸",
    "🇹", "🇺", "🇻", "🇼", "🇽", "🇾", "🇿",
];
const WIN: &str = "👍";
const LOSS: &str = "👎";

pub const ERR_NO_HANGMAN: &str = "No Hangman manager in bot";

pub struct GameRound {
    id: String,
    host: String,
    embed_msg: Option<Message>,
    guesses: Vec<String>,
    game: Hangman,
}

impl GameRound {
    /// Handles a user's guess via emoji reactions
    pub async fn handle_guess(&mut self, ctx: &mut Context, reaction: &Reaction) {
        if let ReactionType::Unicode(s) = &reaction.emoji {
            if let Some(idx) = LETTERS.iter().position(|x| *x == s.as_str()) {
                let guess = char::from_u32(97 + idx as u32).unwrap().to_string();
                if !self.game.has_guessed(&guess) {
                    match self.game.handle_guess(&guess) {
                        Ok(_) => {
                            if !self.game.game_ongoing() {
                                let msg = self.embed_msg.as_ref().unwrap();
                                if self.game.get_attempts() < self.game.get_max_attempts() {
                                    msg.react(&ctx.http, ReactionType::Unicode(String::from(WIN)))
                                        .await
                                        .expect("Failed to react.");
                                } else {
                                    msg.react(&ctx.http, ReactionType::Unicode(String::from(LOSS)))
                                        .await
                                        .expect("Failed to react.");
                                }
                            }
                        }
                        Err(why) => {
                            if let Err(e) = reaction
                                .channel_id
                                .say(
                                    &ctx.http,
                                    format!(
                                        "{}: {} is not a valid guess: {}",
                                        reaction.user_id.unwrap().mention(),
                                        guess,
                                        why
                                    ),
                                )
                                .await
                            {
                                let data = ctx.data.read().await;
                                log(&data, e);
                            }
                        }
                    };
                    self.guesses.push(guess);
                    if let Err(e) = self.update_embed(ctx, reaction.channel_id).await {
                        let data = ctx.data.read().await;
                        log(&data, e);
                    }
                }
            }
        }
    }

    /// Updates the embed showing the game state
    /// # Errors
    /// This method returns an Error if it fails to edit or send any messages
    pub async fn update_embed(&mut self, ctx: &Context, channel: ChannelId) -> CommandResult {
        if self.embed_msg.is_none() {
            let msg = channel
                .send_message(&ctx.http, |m| {
                    m.content("Initializing");
                    m
                })
                .await?;
            self.embed_msg = Some(msg);
        }
        self.guesses.sort();
        let mut ms = self.embed_msg.clone().unwrap();
        ms.edit(&ctx.http, |m| {
            m.content("");
            m.embed(|e| {
                e.title(format!("Hangman! Brought to you by {}.", self.host))
                    .thumbnail(format!(
                        "https://gitlab.com/Arc676/the-carcinizer/-/raw/master/hangman/state{}.png",
                        self.game.get_attempts()
                    ))
                    .field(
                        "Current status".to_string(),
                        self.game.get_current_status().replace('_', "\\_ "),
                        false,
                    )
                    .field(
                        "Incorrect guesses",
                        format!(
                            "{} out of {}",
                            self.game.get_attempts(),
                            self.game.get_max_attempts()
                        ),
                        false,
                    )
                    .field(
                        "Guesses",
                        if self.guesses.is_empty() {
                            "No guesses yet".to_string()
                        } else {
                            self.guesses.join(", ")
                        },
                        false,
                    )
                    .footer(|f| f.text(format!("Game ID: {}", &self.id)))
            })
        })
        .await?;
        Ok(())
    }

    /// Returns the handle of the game's host
    pub fn get_host(&self) -> &String {
        &self.host
    }

    /// Returns whether the game is still in progress
    pub fn game_ongoing(&self) -> bool {
        self.game.game_ongoing()
    }

    /// Returns the secret word of the game
    pub fn get_secret(&self) -> String {
        self.game.get_secret().to_string()
    }
}

fn generate_key() -> String {
    let mut rng = thread_rng();
    (0..12).map(|_| rng.sample(Alphanumeric) as char).collect()
}

#[command]
#[description("Create a new Hangman game with a user-provided secret word or phrase")]
#[usage("secret")]
#[only_in(dm)]
#[min_args(1)]
async fn create_hangman(ctx: &Context, m: &Message, mut args: Args) -> CommandResult {
    let data = ctx.data.write().await;
    let mut errors = Vec::new();
    {
        let mut games = data
            .get::<HangmanManager>()
            .expect(ERR_NO_HANGMAN)
            .games
            .lock()
            .await;
        let id = generate_key();
        let secret = args.single::<String>()?.to_lowercase();
        let max_attempts = args.single::<u32>().unwrap_or(8);
        let host = match m.author.id.to_user(&ctx.http).await {
            Ok(u) => u.name,
            Err(e) => {
                errors.push(e);
                String::from("Unknown user")
            }
        };
        let game = Hangman::new(secret, max_attempts)?;
        if let Err(e) = m.reply(&ctx.http, format!("Created game. Use the following command to start the game in the desired guild channel.\n```\n]start_hangman {}\n```", &id)).await {
            errors.push(e);
        }
        games.insert(
            id.clone(),
            GameRound {
                id,
                host,
                embed_msg: None,
                guesses: vec![],
                game,
            },
        );
    }
    let data = data.downgrade();
    for err in errors {
        log(&data, err);
    }
    Ok(())
}

#[command]
#[description("Starts a game previously created by a user (using the ID provided by the bot)")]
#[usage("game_ID")]
#[only_in(guild)]
#[num_args(1)]
async fn start_hangman(ctx: &Context, m: &Message, mut args: Args) -> CommandResult {
    let game_id = args.single::<String>()?;
    let data = ctx.data.write().await;
    let mut games = data
        .get::<HangmanManager>()
        .expect(ERR_NO_HANGMAN)
        .games
        .lock()
        .await;
    match games.get_mut(&game_id) {
        Some(game) => game.update_embed(ctx, m.channel_id).await?,
        None => {
            m.reply(&ctx.http, "Failed to find game with specified ID")
                .await?;
        }
    }
    Ok(())
}

#[command]
#[description("Starts a new game using a random word from a wordlist chosen by the user")]
#[usage("wordlist")]
#[num_args(1)]
async fn random_hangman(ctx: &Context, m: &Message, mut args: Args) -> CommandResult {
    let data = ctx.data.write().await;
    let manager = data.get::<HangmanManager>().expect(ERR_NO_HANGMAN);
    let list = args.single::<String>().unwrap_or_default();
    let lists = &manager.wordlists;
    if !lists.contains(&list) {
        m.reply(ctx, "This wordlist does not exist").await?;
        return Ok(());
    }
    let mut games = manager.games.lock().await;
    let id = generate_key();
    let max_attempts = args.single::<u32>().unwrap_or(8);
    let host = format!("wordlist '{}'", &list);
    let game = Hangman::new_from_word_list(list, max_attempts)?;
    let mut game = GameRound {
        id: id.clone(),
        host,
        embed_msg: None,
        guesses: vec![],
        game,
    };
    game.update_embed(ctx, m.channel_id).await?;
    games.insert(id, game);
    Ok(())
}

#[command]
#[description("Manage the Hangman wordlists")]
#[usage("((add|remove) wordlist)|(rename wordlist new_name)")]
#[owners_only]
#[min_args(2)]
#[max_args(3)]
async fn wordlist(ctx: &Context, m: &Message, mut args: Args) -> CommandResult {
    let operation = args.single::<String>()?;
    let list_name = args.single::<String>()?;
    let mut data = ctx.data.write().await;
    let lists = &mut data
        .get_mut::<HangmanManager>()
        .expect(ERR_NO_HANGMAN)
        .wordlists;
    match operation.as_str() {
        "rename" => {
            let new_name = args.single::<String>()?;
            match lists.iter().position(|w| *w == list_name) {
                Some(idx) => {
                    lists[idx] = new_name.clone();
                    match fs::rename(&list_name, &new_name) {
                        Ok(_) => {
                            m.reply(
                                ctx,
                                format!("Renamed wordlist '{}' to '{}'", list_name, new_name),
                            )
                            .await?;
                        }
                        Err(e) => {
                            m.reply(ctx, format!("Failed to rename wordlist: {}", e))
                                .await?;
                        }
                    }
                }
                None => {
                    m.reply(ctx, format!("The wordlist '{}' does not exist", list_name))
                        .await?;
                }
            }
        }
        "add" => match m.attachments.first() {
            None => {
                m.reply(ctx, "No wordlist attached").await?;
            }
            Some(attachment) => {
                let contents = match attachment.download().await {
                    Ok(bytes) => bytes,
                    Err(e) => {
                        m.reply(ctx, format!("Failed to download wordlist: {}", e))
                            .await?;
                        return Ok(());
                    }
                };
                match File::create(&list_name) {
                    Ok(mut file) => match file.write_all(&contents) {
                        Ok(_) => {
                            lists.push(list_name.clone());
                            m.reply(ctx, format!("Downloaded wordlist: {}", list_name))
                                .await?;
                        }
                        Err(e) => {
                            m.reply(ctx, format!("Failed to write wordlist to disk: {}", e))
                                .await?;
                        }
                    },
                    Err(e) => {
                        m.reply(ctx, format!("Failed to create file: {}", e))
                            .await?;
                    }
                }
            }
        },
        "remove" => {
            let idx = lists.iter().position(|w| *w == list_name);
            match idx {
                None => {
                    m.reply(ctx, format!("The wordlist '{}' does not exist", list_name))
                        .await?;
                }
                Some(idx) => {
                    lists.remove(idx);
                    match fs::remove_file(&list_name) {
                        Ok(_) => {
                            m.reply(ctx, format!("Removed wordlist '{}'", list_name))
                                .await?;
                        }
                        Err(e) => {
                            m.reply(ctx, format!("Failed to delete wordlist from disk: {}", e))
                                .await?;
                        }
                    }
                }
            }
        }
        _ => {
            m.reply(ctx, format!("Unknown wordlist command '{}'", operation))
                .await?;
        }
    };
    Ok(())
}

#[command]
#[description("Shows available wordlists for Hangman")]
async fn hangman_lists(ctx: &Context, m: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let lists = &data
        .get::<HangmanManager>()
        .expect(ERR_NO_HANGMAN)
        .wordlists;
    m.reply(ctx, format!("Available lists: {}", lists.join(", ")))
        .await?;
    Ok(())
}

pub async fn hangman_handler(ctx: &Context, reaction: &Reaction) {
    let data = ctx.data.write().await;
    let mut res = None;
    {
        let mut games = data
            .get::<HangmanManager>()
            .expect(ERR_NO_HANGMAN)
            .games
            .lock()
            .await;
        if let Ok(msg) = reaction.message(&ctx.http).await {
            if let Some(embed) = msg.embeds.get(0) {
                if let Some(footer) = &embed.footer {
                    let game_id = footer.text.replace("Game ID: ", "");
                    let mut game_over = false;
                    if let Some(round) = games.get_mut(&game_id) {
                        let mut mctx = ctx.clone();
                        round.handle_guess(&mut mctx, reaction).await;
                        game_over = !round.game_ongoing();
                    }
                    if game_over {
                        {
                            let round = &games[&game_id];
                            if let Err(e) = reaction
                                .channel_id
                                .say(
                                    &ctx.http,
                                    format!(
                                        "Game with ID {} by {} has ended. The word was {}.",
                                        game_id,
                                        round.get_host(),
                                        round.get_secret()
                                    ),
                                )
                                .await
                            {
                                res = Some(e);
                            }
                        }
                        games.remove(&game_id);
                    }
                };
            }
        }
    }
    if let Some(e) = res {
        log(&data.downgrade(), e);
    }
}
