// Copyright (C) 2022 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3).

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use chrono::Local;
use std::collections::HashSet;
use std::fmt::Display;
use std::fs;
use std::fs::File;
use std::io::{Read, Write};
use std::str::FromStr;
use std::sync::Arc;

use serde::{Deserialize, Serialize};

use serenity::async_trait;
use serenity::framework::standard::macros::{check, help, hook};
use serenity::framework::standard::{
    help_commands, Args, CommandGroup, CommandResult, DispatchError, HelpOptions, Reason,
    StandardFramework,
};
use serenity::model::gateway::Ready;
use serenity::model::prelude::{ChannelId, Message, Reaction, UserId};
use serenity::prelude::*;
use tokio::sync::RwLockReadGuard;

use crate::counting::*;
use crate::drawing::*;
use crate::hangman::*;
use crate::plates::*;
use crate::util::*;

mod counting;
mod drawing;
mod hangman;
mod plate_data;
mod plates;
mod util;

const BOT_PREFIX: &str = "]";
const ERR_NO_CONFIG: &str = "No config data found in bot";

#[derive(Serialize, Deserialize)]
struct BotConfig {
    admins: Vec<u64>,
    logs: String,
    wake: Vec<ChannelId>,
    sha: String,
    state: String,
}

impl TypeMapKey for BotConfig {
    type Value = BotConfig;
}

impl Default for BotConfig {
    fn default() -> Self {
        BotConfig {
            admins: vec![],
            logs: String::from("carcinizer.log"),
            wake: vec![],
            sha: String::new(),
            state: String::new(),
        }
    }
}

#[derive(Default, Serialize, Deserialize)]
struct BotState {
    count: CountSetup,
    hangman: HangmanManager,
    drawing: DrawingManager,
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, mut msg: Message) {
        ping_handler(&ctx, &mut msg).await;
        count_handler(&ctx, &msg).await;
        check_projects(&ctx).await;
    }

    async fn reaction_add(&self, ctx: Context, reaction: Reaction) {
        hangman_handler(&ctx, &reaction).await;
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        let data = ctx.data.read().await;
        log(&data, format!("Logged in as {}", ready.user.name));

        // Announce bot version, if configured
        let config = data.get::<BotConfig>().expect(ERR_NO_CONFIG);
        let commit = if let Ok(mut file) = File::open(&config.sha) {
            let mut sha = String::new();
            match file.read_to_string(&mut sha) {
                Ok(_) => sha,
                Err(e) => {
                    eprintln!("Failed to read commit SHA: {}", e);
                    "(failed to read)".to_string()
                }
            }
        } else {
            "(unknown)".to_string()
        };
        let msg = format!(
            "Running version {} on commit `{}`",
            env!("CARGO_PKG_VERSION"),
            commit
        );
        for channel in &config.wake {
            if let Err(e) = channel.say(&ctx.http, &msg).await {
                eprintln!("Failed to send launch message: {}", e);
            }
        }

        let channel = config.wake.first().copied();
        drop(data);
        init_counting(&ctx, channel).await;

        if let Err(e) = load_projects(&ctx).await {
            eprintln!("Failed to load drawing projects: {}", e);
        }
        init_drawing(&ctx).await;
    }
}

fn log<S: Display>(data: &RwLockReadGuard<TypeMap>, content: S) {
    let now = Local::now().format("%F %T %Z").to_string();
    let file = &data.get::<BotConfig>().expect(ERR_NO_CONFIG).logs;
    match File::options().append(true).create(true).open(file) {
        Ok(mut f) => {
            if let Err(e) = f.write(format!("[{}] {}\n", now, content).as_ref()) {
                eprintln!("[{}] Failed to log incident: {} ({})", now, content, e);
            }
        }
        Err(e) => eprintln!("[{}] Failed to log incident: {} ({})", now, content, e),
    }
}

async fn save_state(ctx: &Context) {
    let data = ctx.data.read().await;
    let config = data.get::<BotConfig>().expect(ERR_NO_CONFIG);
    let count = data.get::<CountSetup>().expect(ERR_NO_COUNTING).clone();
    let hangman = data.get::<HangmanManager>().expect(ERR_NO_HANGMAN).clone();
    let drawing = data.get::<DrawingManager>().expect(ERR_NO_DRAWING).clone();
    let state = BotState {
        count,
        hangman,
        drawing,
    };
    match File::create(&config.state) {
        Ok(file) => {
            if let Err(e) = serde_json::to_writer(file, &state) {
                log(&data, format!("Failed to save bot state to disk: {}", e));
            }
        }
        Err(e) => {
            log(&data, format!("Failed to open bot state file: {}", e));
        }
    }
}

fn parse_id<T: FromStr>(args: &mut Args) -> Result<T, &str> {
    match args.single::<T>() {
        Ok(value) => Ok(value),
        Err(_) => {
            let input = args
                .single::<String>()
                .expect("Failed to read string argument");
            let len = input.len();
            if len < 4 {
                return Err("Invalid ID");
            }
            match (&input[2..len - 1]).parse::<T>() {
                Ok(id) => Ok(id),
                Err(_) => Err("Failed to parse ID"),
            }
        }
    }
}

#[check]
#[name("Accountability")]
async fn report_incident(ctx: &Context, msg: &Message) -> Result<(), Reason> {
    let data = ctx.data.read().await;
    let author = &msg.author;
    log(
        &data,
        format!(
            "User {}#{} ({}) used command: {}",
            author.name, author.discriminator, author.id, msg.content
        ),
    );
    Ok(())
}

async fn is_owner(ctx: &Context, uid: UserId) -> bool {
    let data = ctx.data.read().await;
    let config = data.get::<BotConfig>().expect(ERR_NO_CONFIG);
    config.admins.contains(uid.as_u64())
}

#[tokio::main]
async fn main() {
    let config = if let Ok(file) = File::open("carcinizer.json") {
        match serde_json::from_reader(file) {
            Ok(value) => value,
            Err(e) => {
                eprintln!("Failed to read config: {}", e);
                BotConfig::default()
            }
        }
    } else {
        BotConfig::default()
    };

    let state = if let Ok(file) = File::open(&config.state) {
        match serde_json::from_reader(file) {
            Ok(value) => value,
            Err(e) => {
                eprintln!("Failed to read bot state: {}", e);
                BotState::default()
            }
        }
    } else {
        BotState::default()
    };

    let admins = config.admins.iter().map(|x| UserId(*x)).collect();
    let framework = StandardFramework::new()
        .configure(|c| c.prefix(BOT_PREFIX).owners(admins))
        .help(&HELP)
        .after(post_check)
        .on_dispatch_error(dispatch_error)
        .group(&COUNTING_GROUP)
        .group(&PLATES_GROUP)
        .group(&EXECUTIONER_GROUP)
        .group(&DRAWING_GROUP)
        .group(&GENERAL_GROUP);

    let token = fs::read_to_string(".token").expect("Failed to read bot token from disk");
    let intents = GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(token, intents)
        .event_handler(Handler)
        .framework(framework)
        .type_map_insert::<CountSetup>(state.count)
        .type_map_insert::<LatencyMeasurement>(LatencyMeasurement::default())
        .type_map_insert::<BotConfig>(config)
        .type_map_insert::<HangmanManager>(state.hangman)
        .type_map_insert::<DrawingManager>(state.drawing)
        .await
        .expect("Error creating client");

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(Arc::clone(&client.shard_manager));
    }

    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[hook]
async fn dispatch_error(ctx: &Context, msg: &Message, error: DispatchError, command_name: &str) {
    let response = match error {
        DispatchError::CheckFailed(s, reason) => {
            let data = ctx.data.read().await;
            log(
                &data,
                format!("Check for command {} failed with: {}", command_name, s),
            );
            match reason {
                Reason::User(r) => {
                    log(
                        &data,
                        format!("Check for command {} failed due to: {}", command_name, r),
                    );
                    format!("Checks failed: {}. This incident will be logged.", r)
                }
                Reason::Log(r) => {
                    log(
                        &data,
                        format!("Check for command {} failed due to: {}", command_name, r),
                    );
                    "Command checks failed. Ask an admin for more information. This incident will be logged.".to_string()
                }
                Reason::UserAndLog { user, log: rl } => {
                    log(
                        &data,
                        format!(
                            "Check for command {} failed. User info: {}. Log info: {}",
                            command_name, user, rl
                        ),
                    );
                    format!("Checks failed: {}. This incident will be logged.", user)
                }
                _ => {
                    log(
                        &data,
                        format!(
                            "Check for command {} failed for an unknown reason",
                            command_name
                        ),
                    );
                    "Command checks failed for an unknown reason. Please inform an admin. This incident will be logged.".to_string()
                }
            }
        }
        DispatchError::Ratelimited(info) => {
            if info.is_first_try {
                let time = info.rate_limit.as_secs();
                format!(
                    "Command failed due to rate-limiting. Please try again in {} seconds",
                    time
                )
            } else {
                let data = ctx.data.read().await;
                let author = &msg.author;
                log(
                    &data,
                    format!(
                        "User {}#{} ({}) exceeded rate-limit of command {} more than once",
                        author.name, author.discriminator, author.id, command_name
                    ),
                );
                "The rate-limiting duration has not yet elapsed. This incident will be logged"
                    .to_string()
            }
        }
        DispatchError::CommandDisabled => {
            let data = ctx.data.read().await;
            log(
                &data,
                format!("User attempted to use disabled command {}", command_name),
            );
            "Looks like this command has been disabled. Contact an admin if you think this is a mistake.".to_string()
        }
        DispatchError::BlockedUser
        | DispatchError::BlockedGuild
        | DispatchError::BlockedChannel => return,
        DispatchError::OnlyForDM => "This command can only be used in DMs.".to_string(),
        DispatchError::OnlyForGuilds => "This command can only be used in guilds.".to_string(),
        DispatchError::OnlyForOwners => {
            let data = ctx.data.read().await;
            let author = &msg.author;
            log(
                &data,
                format!(
                    "User {}#{} ({}) attempted to use command {}",
                    author.name, author.discriminator, author.id, command_name
                ),
            );
            "This command can only be used by admins. This incident will be logged.".to_string()
        }
        DispatchError::LackingRole => format!(
            "This command requires (a) certain role(s). Try ]help {} for more information.",
            command_name
        ),
        DispatchError::LackingPermissions(perms) => {
            let data = ctx.data.read().await;
            let author = &msg.author;
            log(
                &data,
                format!(
                    "User {}#{} ({}) attempted to use command {}",
                    author.name, author.discriminator, author.id, command_name
                ),
            );
            format!("This command requires the following permissions: {}. This incident will be logged.",
                    perms.get_permission_names().join(", "))
        }
        DispatchError::NotEnoughArguments { min, given } => format!(
            "Command `{}` requires {} argument(s) but {} were given.",
            command_name, min, given
        ),
        DispatchError::TooManyArguments { max, given } => format!(
            "Command `{}` takes at most {} argument(s) but {} were given.",
            command_name, max, given
        ),
        _ => format!(
            "Unhandled dispatch error in `{}`. Please inform an admin.",
            command_name
        ),
    };
    if let Err(e) = msg.reply(ctx, response).await {
        let data = ctx.data.read().await;
        log(&data, e);
    }
}

#[hook]
async fn post_check(ctx: &Context, _: &Message, cmd: &str, res: CommandResult) {
    match res {
        Ok(_) => {
            save_state(ctx).await;
        }
        Err(e) => {
            let data = ctx.data.read().await;
            log(&data, format!("Command '{}' failed: {}", cmd, e));
        }
    }
}

#[help]
async fn help(
    ctx: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    let _ = help_commands::with_embeds(ctx, msg, args, help_options, groups, owners).await;
    Ok(())
}
