// Copyright (C) 2022 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3).

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use crate::plate_data::*;
use regex::RegexBuilder;
use serenity::client::Context;
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{Args, CommandResult};
use serenity::model::prelude::Message;
use serenity::utils::Colour;

type Color = Colour;

#[group]
#[commands(plate)]
struct Plates;

fn search(space: &[&str], query: &str) -> Result<String, String> {
    let mut results = String::new();
    let mut builder = RegexBuilder::new(&*format!("(?i){}", query));
    let builder = builder.size_limit(1 << 20);
    let re = match builder.build() {
        Ok(re) => re,
        Err(e) => return Err(format!("Invalid regex: {}", e)),
    };
    for item in space {
        if re.is_match(item) {
            results.push_str(item);
            results.push('\n');
        }
    }
    Ok(results)
}

#[command]
#[description(
    "Searches for a license plate code in the selected search spaces by regex (no look-around or back-references). Case insensitive by default. Available search spaces:\n\
CE - Embassies in Beijing, China\n\
CP - Provinces in China\n\
IT - Provinces in Italy\n\
GD - Diplomat vehicles in Germany\n\
GK - German Kraftfahrzeug-Zulassungsbehörden\n\
CH - Swiss cantons\n\
FR - French departments"
)]
#[usage("search_space query/regex...")]
#[min_args(2)]
async fn plate(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let space: String = args.single()?;
    let query = args.rest();
    let (results, space_name) = match space.to_uppercase().as_str() {
        "CE" => (search(&PLATES_CE, query), "Embassies in Beijing"),
        "CP" => (search(&PLATES_CP, query), "Chinese Provinces"),
        "IT" => (search(&PLATES_IP, query), "Italian Provinces"),
        "GD" => (search(&PLATES_GE, query), "Diplomat Vehicles in Germany"),
        "GK" => (
            search(&PLATES_KFZ, query),
            "German Kraftfahrzeug-Zulassungsbehörden",
        ),
        "CH" => (search(&PLATES_CH, query), "Swiss Cantons"),
        "FR" => (search(&PLATES_FR, query), "French Departments"),
        _ => {
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    m.reference_message(msg).embed(|e| {
                        e.title("Search failed")
                            .description(format!("Invalid search space \"{}\"", space))
                            .color(Color::RED)
                    })
                })
                .await?;
            return Ok(());
        }
    };
    match results {
        Err(err) => {
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    m.reference_message(msg)
                        .embed(|e| e.title("Search failed").description(err).color(Color::RED))
                })
                .await?;
        }
        Ok(results) => {
            if results.is_empty() {
                msg.channel_id
                    .send_message(&ctx.http, |m| {
                        m.reference_message(msg).embed(|e| {
                            e.title("Search complete")
                                .description("No results found for your query")
                                .color(Color::GOLD)
                        })
                    })
                    .await?;
            } else {
                msg.channel_id
                    .send_message(&ctx.http, |m| {
                        m.reference_message(msg).embed(|e| {
                            e.title("Search results")
                                .description("Your query yielded the following results")
                                .field(space_name, results, false)
                                .color(Color::from_rgb(0, 255, 0))
                        })
                    })
                    .await?;
            }
        }
    }
    Ok(())
}
