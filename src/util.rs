// Copyright (C) 2022 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3).

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use crate::{log, parse_id, ACCOUNTABILITY_CHECK};
use chrono::Utc;

use serde_json::Value;
use serenity::client::bridge::gateway::{ShardId, ShardManager};
use serenity::client::Context;
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{Args, CommandResult};
use serenity::model::prelude::{ChannelId, Message, MessageId};
use serenity::prelude::TypeMapKey;

use std::fmt::Write;
use std::sync::Arc;
use tokio::sync::Mutex;

#[group]
#[owner_privilege]
#[commands(define, ping, say, source)]
struct General;

const ERR_NO_MEASUREMENT: &str = "No latency measurement setup found in bot";
const ERR_SHARD_MGR: &str = "Failed to obtain shard manager";
const ERR_SHARD: &str = "Failed to obtain shard";
const ERR_DURATION: &str = "Failed to obtain shard runner latency";

#[derive(Default)]
pub struct LatencyMeasurement {
    checks: Vec<(MessageId, String, i64)>,
}

impl TypeMapKey for LatencyMeasurement {
    type Value = LatencyMeasurement;
}

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

#[command]
#[description("Shows latency information for the bot's connection")]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    let mut data = ctx.data.write().await;

    let heartbeat = match data.get::<ShardManagerContainer>() {
        Some(manager) => {
            let mgr = manager.lock().await;
            let runners = mgr.runners.lock().await;
            match runners.get(&ShardId(ctx.shard_id)) {
                Some(runner) => match runner.latency {
                    Some(duration) => format!("{}ms", duration.as_millis()),
                    None => String::from(ERR_DURATION),
                },
                None => String::from(ERR_SHARD),
            }
        }
        None => String::from(ERR_SHARD_MGR),
    };

    let pings = data
        .get_mut::<LatencyMeasurement>()
        .expect(ERR_NO_MEASUREMENT);

    let now = Utc::now().timestamp_millis();
    let sent = msg
        .channel_id
        .send_message(&ctx.http, |m| {
            m.reference_message(msg).embed(|e| {
                e.title("Pong! :table_tennis:")
                    .field("Shard latency", heartbeat.clone(), false)
                    .field("Message latency", "Timing...", false)
            })
        })
        .await?
        .id;

    pings.checks.push((sent, heartbeat, now));
    Ok(())
}

pub async fn ping_handler(ctx: &Context, msg: &mut Message) {
    let now = Utc::now().timestamp_millis();
    let mut data = ctx.data.write().await;
    let pings = data
        .get_mut::<LatencyMeasurement>()
        .expect(ERR_NO_MEASUREMENT);
    let mut idx = None;
    let mut res = Ok(());
    for (i, (id, heartbeat, time)) in pings.checks.iter().enumerate() {
        if msg.id == *id {
            let dt = now - time;
            res = msg
                .edit(&ctx, |m| {
                    m.embed(|e| {
                        e.title("Pong! :table_tennis:")
                            .field("Shard latency", heartbeat, false)
                            .field("Message latency", format!("{}ms", dt), false)
                    })
                })
                .await;
            idx = Some(i);
            break;
        }
    }
    if let Some(idx) = idx {
        pings.checks.remove(idx);
    }
    if let Err(e) = res {
        log(&data.downgrade(), e);
    }
}

#[command]
#[description("Echoes a message in a specified channel. Uses of this command are logged.")]
#[usage("#channel message...")]
#[checks(Accountability)]
#[owner_privilege]
#[min_args(2)]
async fn say(ctx: &Context, _: &Message, mut args: Args) -> CommandResult {
    let channel: ChannelId = parse_id(&mut args)?;
    let msg = args.rest();
    let _ = channel.say(&ctx.http, msg).await;
    Ok(())
}

#[command]
#[description("Provides a link to the source code")]
async fn source(ctx: &Context, msg: &Message) -> CommandResult {
    let url = ctx.cache.current_user().avatar_url();
    let found_url = url.is_some();
    let data = ctx.data.read().await;
    msg.channel_id
        .send_message(&ctx.http, |m| {
            let cm = m.embed(|e| {
                e.title("Bot Info")
                    .thumbnail(url.unwrap_or_else(|| "attachment://Bot_icon.png".to_string()))
                    .field("Bot version", env!("CARGO_PKG_VERSION"), false)
                    .field(
                        "Source code",
                        "https://gitlab.com/Arc676/the-carcinizer",
                        false,
                    )
            });
            if found_url {
                cm
            } else {
                log(&data, "Failed to obtain bot avatar URL");
                cm.add_file("./res/Bot icon.png")
            }
        })
        .await?;
    Ok(())
}

#[command]
#[num_args(1)]
#[description("Looks up the definition of a word using the Free Dictionary API")]
#[usage("word")]
async fn define(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let word = args.single::<String>()?;
    let mut valid = true;
    for c in word.chars() {
        if !c.is_ascii_alphabetic() {
            valid = false;
            break;
        }
    }
    if !valid {
        msg.reply(ctx, "ASCII-alphabetic words only").await?;
        return Ok(());
    }
    let response = reqwest::get(format!(
        "https://api.dictionaryapi.dev/api/v2/entries/en/{}",
        word
    ))
    .await?
    .text()
    .await?;
    match serde_json::from_str::<Value>(&response) {
        Ok(definitions) => {
            if let Some(definitions) = definitions.as_array() {
                msg.channel_id
                    .send_message(&ctx.http, |m| {
                        m.reference_message(msg).embed(|e| {
                            e.title(format!("Definitions for {}", word))
                                .description("Brought to you by https://dictionaryapi.dev/");
                            let mut trimmed = false;
                            for inner in definitions {
                                if let Some(meanings) =
                                    inner.get("meanings").and_then(|m| m.as_array())
                                {
                                    for meaning in meanings {
                                        let category = meaning
                                            .get("partOfSpeech")
                                            .and_then(|v| v.as_str())
                                            .unwrap_or("Unknown part of speech");
                                        let definitions = if let Some(definitions) =
                                            meaning.get("definitions").and_then(|d| d.as_array())
                                        {
                                            definitions
                                                .iter()
                                                .map(|x| {
                                                    x.get("definition").and_then(|v| v.as_str())
                                                })
                                                .enumerate()
                                                .fold(String::new(), |mut acc, (i, x)| {
                                                    if let Some(def) = x {
                                                        if acc.len() + def.len() <= 1019 {
                                                            let _ = write!(
                                                                acc,
                                                                "{}{}. {}",
                                                                if acc.is_empty() {
                                                                    ""
                                                                } else {
                                                                    "\n"
                                                                },
                                                                i + 1,
                                                                def
                                                            );
                                                        } else {
                                                            trimmed = true;
                                                        }
                                                    }
                                                    acc
                                                })
                                        } else {
                                            "Failed to read definitions".to_string()
                                        };
                                        e.field(category, definitions, false);
                                    }
                                }
                            }
                            if trimmed {
                                e.footer(|f| {
                                    f.text("Some definitions were omitted due to embed field length limitations")
                                });
                            }
                            e
                        })
                    })
                    .await?;
            } else {
                msg.reply(ctx, "No definitions found").await?;
            }
        }
        Err(e) => {
            msg.reply(ctx, "Failed to parse API response").await?;
            let data = ctx.data.read().await;
            log(&data, format!("Failed to parse API response: {}", e));
            log(&data, format!("API response: {}", response));
        }
    }
    Ok(())
}
